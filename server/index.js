const express = require('express');
const app = express()
const bodyParser = require('body-parser');

const Queries = require('./queries').Queries;
const Instamojo = require('./instamojo').Instamojo;
const Boxes = require('./config').Boxes;

app.use(bodyParser.json())
app.use(express.static('public'));

app.get('/', (req, res) => {
    res.sendFile('index.html')
})
        
app.post('/instamojo/url', (req, res) => {
    console.log(req.body);
    let user_details = req.body;

    //since box_type is extra for dB columns
    let box_type = req.body.box_type;
    delete user_details.box_type;

    Queries.storeUserDetails(user_details)
        .then(function (result) {
            let order_details = {};
            order_details.user_id = result[0];
            Instamojo.getUrl(user_details, box_type, function (instamojo_response) {
               order_details.payment_request_id = instamojo_response.id;
               order_details.payment_status = instamojo_response.status;
               order_details.box_type = Boxes[box_type].id;
               Queries.storeOrderDetails(order_details)
               .then(() => {
                   res.send(instamojo_response.longurl);
               })
            });
        });;
});

app.post('/instamojo/redirect', (req, res) => {
    Queries.storePaymentDetails(req.query)
    .then(function (result){
        res.send({
            res: 'success',
            id: result
        });
    });
});

app.post('/questionare', function(req, res) {
    
});

app.listen(process.env.PORT || 3000, () => console.log('Example app listening on port 3000!'));