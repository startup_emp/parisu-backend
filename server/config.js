exports.config = {
   host : 'https://parisu.co.in'
}

exports.Boxes = {
    kids : {
        id : 1,
        amount : 1000,
        purpose: 'Kids Box'
    },

    enterprener: {
        id: 2,
        amount: 1000,
        purpose: 'Enterpreneur Box'
    },

    surprise: {
        id: 3,
        amount: 1000,
        purpose: 'Surprise Box'
    },

    classic: {
        id: 4,
        amount: 1000,
        purpose: 'Classic Box'
    },

    newmom: {
        id: 5,
        amount: 1000,
        purpose: 'New Mom\'s Box'
    }
}