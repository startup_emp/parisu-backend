var request = require('request');
const config = require("./config").config;
const Boxes = require('./config').Boxes;
const default_options = {
    redirect_url: `${config.host}/instamojo/redirect`,
    allow_repeated_payments: false
}

const header = {
    'X-Api-Key': '2624773a557f3689a1d5228e28d0f660',
    'X-Auth-Token': '07418cba576e4e949f85c402f8e8a79d'
}

exports.Instamojo = {
    API : {
        host: 'https://www.instamojo.com/api/1.1',
        createRequest: '/payment-requests/',
        getCreateRequestUrl : function() {
            return this.host + this.createRequest;
        }
    },
    
    getUrl : function(options, box_type, callback) {
        let instamojo_options = {
            form: { ...default_options,
                ...options,
                ...Boxes[box_type]
            },
            headers: header
        };
        request.post(this.API.getCreateRequestUrl(), instamojo_options, function (error, response, body) {
            if (!error && response.statusCode == 201) {
                let instamojo_response = JSON.parse(body).payment_request;
                console.log(instamojo_response);
                callback(instamojo_response);
            }
            else{
                console.log(error);
            }
        })
    }
}