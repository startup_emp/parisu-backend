let knex = require('knex')({
    client: 'mysql',
    connection: {
        host: '127.0.0.1',
        user: 'root',
        password: '',
        database: 'parisu_db'
    }
});

exports.Queries = {
    tables : {
        user_details: 'user_details',
        order_details : 'order_details'
    },

    columns : {
        order_details : {
            payment_request_id: 'payment_request_id',
            payment_id: 'payment_id',
            payment_status: 'payment_status'
        }
    },

    storeUserDetails: function(data) {
        return knex(this.tables.user_details).insert(data);
    },

    storeOrderDetails: function(data) {
        return knex(this.tables.order_details).insert(data);
    },

    storePaymentDetails : function(data) {
        return knex(this.tables.order_details)
            .where(this.columns.order_details.payment_request_id, '=', data.payment_request_id)
            .update({
                payment_id: data.payment_id,
                payment_status: data.payment_status
            });
    }
}