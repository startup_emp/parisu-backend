let PopUp = {
    details : {},

    Templates: {
        skeliton: Handlebars.templates.skeliton(),
        personal: Handlebars.templates.personal(),
        delivery: Handlebars.templates.delivery(),
        categories: Handlebars.templates.categories(),
        questionare: Handlebars.templates.questionare()
    },

    requestType : {
        POST : 'post'
    },

    URL : {
        createUrl : '/instamojo/url',
        questionare: '/questionare'
    },

    API: {
        getSubscriptionUrl: function () {
            let ajaxDetails = {
                url: PopUp.URL.createUrl,
                data: JSON.parse(localStorage.getItem('parisu_details')),
                dataType: 'json',
                type: PopUp.requestType.POST,
                success: function(result, status, xhr){
                    window.open(result.instamojoUrl);
                }
            }
            $.ajax(ajaxDetails).done(function(result){

            });
            localStorage.clear();
        },

        saveAdditionalDetails: function (formId) {

        }
    },

    Utilities: {
        showPopForm: function (page) {
            if (!document.getElementById('popup-inner')) {
                $('#footer').after(PopUp.Templates.skeliton);
            }
            switch (page) {
                case 'personal':
                    $('#popup-inner').html(PopUp.Templates.personal);
                    break;

                case 'delivery':
                    $('#popup-inner').html(PopUp.Templates.delivery);
                    break;

                case 'categories':
                    $('#popup-inner').html(PopUp.Templates.categories);
                    break;

                case 'questionare':
                    $('#popup-inner').html(PopUp.Templates.questionare);
                    break;
            }
        },

        showPopUp: function(page, boxType){
            PopUp.details.boxType = boxType;
            localStorage.setItem("parisu_details", JSON.stringify(PopUp.details));
            this.showPopForm(page);
        },

        savePersonalDetails: function (formId) {
            let formArray = $(`#${formId}`).serializeArray();
            formArray.forEach(element => {
                PopUp.details[element.name] = element.value;
            })
            localStorage.setItem("parisu_details", JSON.stringify(PopUp.details));
        },

        saveDeliveryDetails: function (formId) {
            let formArray = $(`#${formId}`).serializeArray();
            formArray.forEach(element => {
                PopUp.details[element.name] = element.value;
            })
            localStorage.setItem("parisu_details", JSON.stringify(PopUp.details));
        },

        openSubscriptionPage: function(){
            let subscriptionUrl = PopUp.API.getSubscriptionUrl();
            //window.open(subscriptionUrl, '_blank');
        }
    }
}