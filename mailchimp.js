const request = require('request');

function addSubscriber(email, firstname, lastname) {
    let form = {
        method: 'POST',
        url: 'https://us19.api.mailchimp.com/3.0/lists/2db71d7cc4/members',
        headers: {
            'Postman-Token': 'a6a2000c-f5c4-4621-8715-29de6723df68',
            'Cache-Control': 'no-cache',
            Authorization: 'Bearer 34ac37281fe8c0460ce84b397a6cdeed-us19',
            'Content-Type': 'application/json'
        },
        body: {
            email_address: email,
            status: 'subscribed',
            merge_fields: {
                FNAME: firstname,
                LNAME: lastname
            }
        },
        json: true
    };

    request(form, function (error, response, body) {
        if (error) throw new Error(error);
        console.log(body)

    });
}
module.exports = addSubscriber;